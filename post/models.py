from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    image = models.ImageField((upload_to='media')
    content = models.TextField()
    created_at = models.DateField()

    def __str__(self):
        return content